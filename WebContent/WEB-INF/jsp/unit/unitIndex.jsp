<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>聊天室 - editor:yinq</title>
<link rel="shortcut icon" href="favicon.png">
<link type="text/css" rel="stylesheet" href="../css/style.css">
<script type="text/javascript" src="../js/jquery.min.js"></script>
</head>

<body>
<div class="chatbox">
  <div class="chat_top fn-clear">
    <div class="logo"><img src="../images/logo.png" width="190" height="60"  alt=""/></div>
    <div class="uinfo fn-clear">
      <div class="uface"><img src="../images/hetu.jpg" width="40" height="40"  alt=""/></div>
    </div>
  </div>
  <div class="chat_message fn-clear">
    <div class="chat_left">
      <div class="message_box" id="message_box">
        
<!--         <div class="msg_item fn-clear"> -->
<!--           <div class="uface"><img src="../images/53f442834079a.jpg" width="40" height="40"  alt=""/></div> -->
<!--           <div class="item_right"> -->
<!--             <div class="msg">请问有什么需要帮助的吗？</div> -->
<!--             <div class="name_time">机器人 · 1分钟前</div> -->
<!--           </div> -->
<!--         </div> -->
        
<!--         <div class="msg_item fn-clear"> -->
<!--           <div class="uface"><img src="../images/hetu.jpg" width="40" height="40"  alt=""/></div> -->
<!--           <div class="item_right"> -->
<!--             <div class="msg own">下雨么</div> -->
<!--             <div class="name_time">河图 · 30秒前</div> -->
<!--           </div> -->
<!--         </div> -->
        
      </div>
      
      <div class="write_box">
        <textarea id="message" name="message" class="write_area" placeholder="说点啥吧..."></textarea>
        <input type="hidden" name="fromname" id="fromname" value="荒墨丶迷失" />
        <input type="hidden" name="session_id" id=session_id value="" />
        <div class="facebox fn-clear">
          <div class="expression"></div>
<!--           <div class="chat_type" id="chat_type">群聊</div> -->
          <button name="" class="sub_but">提 交</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$('#message_box').scrollTop($("#message_box")[0].scrollHeight + 20);
	$('.uname').hover(
	    function(){
		    $('.managerbox').stop(true, true).slideDown(100);
	    },
		function(){
		    $('.managerbox').stop(true, true).slideUp(100);
		}
	);
	
	var fromname = $('#fromname').val();
	//提交消息
	$('.sub_but').click(function(event){
	    sendMessage(event, fromname); //调用消息函数
	});
	/*按下按钮或键盘按键*/
	$("#message").keydown(function(event){
		var e = window.event || event;
        var k = e.keyCode || e.which || e.charCode;
		//按下ctrl+enter发送消息
		if((event.ctrlKey && (k == 13 || k == 10) )){
			sendMessage(event, fromname);
		}
	});
});

//发送消息
function sendMessage(event, from_name){
	//获得发送的消息
    var msg = $("#message").val();
// 	alert(msg);
    //自己回复的样式加载
	var htmlData =   '<div class="msg_item fn-clear">'
                   + '   <div class="uface"><img src="../images/hetu.jpg" width="40" height="40"  alt=""/></div>'
			       + '   <div class="item_right">'
			       + '     <div class="msg own">' + msg + '</div>'
			       + '     <div class="name_time">' + from_name + ' · 30秒前</div>'
			       + '   </div>'
			       + '</div>';
	$("#message_box").append(htmlData);
	$('#message_box').scrollTop($("#message_box")[0].scrollHeight + 20);
	$("#message").val('');
	
	var session_id = $("#session_id").val();
	$.ajax({
   		type:"POST",
   		url:"${pageContext.request.contextPath}/unit/common.do",
//    	datatype: "json",
   		data:{
   			"session_id":session_id,
   			"msg":msg
   		},
   		success:function(data){
	   		var mes = eval(data);
	   		if (mes.success) {
	   			var msg = data.msg;
	   			var session_id = data.session_id;
	   			$("#session_id").val(session_id);
	   		 	//机器人回复样式加载
	   			var htmlData =   '<div class="msg_item fn-clear">'
	   		                   + '   <div class="uface"><img src="../images/53f442834079a.jpg" width="40" height="40"  alt=""/></div>'
	   					       + '   <div class="item_right">'
	   					       + '     <div class="msg">' + msg + '</div>'
	   					       + '     <div class="name_time">' + '机器人 ' + '· 30秒前</div>'
	   					       + '   </div>'
	   					       + '</div>';
	   			$("#message_box").append(htmlData);
	   			$('#message_box').scrollTop($("#message_box")[0].scrollHeight + 20);
	   			$("#message").val('');
	   			
	   		} else {
	   			alert("返回数据失败");
	   		}
   		},
   		error: function(){
    		//请求出错处理
    		alert("出情况了");
    		}         
  		});
}

</script>
</body>
</html>
