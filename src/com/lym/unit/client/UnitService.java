package com.lym.unit.client;

import java.util.HashMap;
import java.util.Map;

import com.lym.unit.util.HttpUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 *  unit 核心类
 *@author liyingming
 *@data 2017-11-12
 */
public class UnitService {

    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     */
	 public static void main(String[] args) {
		 utterance("", "下雨么");
	 }
	
    public static Map<String, String> utterance(String session_id,String content){
    	Map<String, String> map = new HashMap<String,String>();
    	try {
	    	String talkUrl = "https://aip.baidubce.com/rpc/2.0/solution/v1/unit_utterance";
	         // 请求参数 scene_id记得修改
	     	String params = "{\"scene_id\":12143,\"session_id\":\"" + session_id + "\",\"query\":\"" + content + "\"}";
	        String accessToken = "";
	        //获得返回的json数据
	        String str = HttpUtil.post(talkUrl, accessToken, "application/json", params);
	     	System.out.println(str);
	     	//获得result
	     	JSONObject jsonobject = JSONObject.fromObject(str);
	     	String result = jsonobject.getString("result");
	     	System.out.println(result);
	     	//获得会话session_id
	    	JSONObject jsonobject2 = JSONObject.fromObject(result);
			session_id = jsonobject2.getString("session_id");
			map.put("session_id", session_id);
			//获得动作列表
			String action_list =jsonobject2.getString("action_list");
			JSONArray json = JSONArray.fromObject(action_list); // 首先把字符串转成 JSONArray  对象
			if(json.size()>0){
			    for(int i=0;i<json.size();i++){
			        JSONObject job = json.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
			        String say = (String) job.get("say");//机器bot返回的话术
			        map.put("say", say);
			        System.out.println(say);
				    String action_id = (String) job.get("action_id");//动作名称，当为fail_action时表示未能命中任何意图
				    map.put("action_id", action_id);
				    if(!"fail_action".equals(action_id)){//命中意图
					    Object object = job.get("action_type");//获得action_type对象  动作详细
				        JSONObject jsona = JSONObject.fromObject(object);//转成json对象
				        String act_type = jsona.getString("act_type");//动作类型，取值clarify/satisfy/guide
		//				    clarify： 澄清 ; satisfy： 满足 ; guide： 引导 ;faqguide： faq引导
		//			    info.setAct_type(act_type);
				        map.put("act_type", act_type);
				        if("satisfy".equals(act_type)){
				    	    //解析意图概要
				    	    String schema =jsonobject2.getString("schema");//获得意图概要
				    	    JSONObject jsonObect = JSONObject.fromObject(schema);
				    	    String current_qu_intent = jsonObect.getString("current_qu_intent");//获得当前意图
				    	    String bot_merged_slots = jsonObect.getString("bot_merged_slots");//词槽列表  
				    	    map.put("current_qu_intent", current_qu_intent);
				    	    map.put("bot_merged_slots", bot_merged_slots);
				        }else{
				        	map.put("mapSchema", null);
				        }
				    }else{
				    	map.put("act_type", null);
				    }
			    }
			}
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return map;
    }


}
