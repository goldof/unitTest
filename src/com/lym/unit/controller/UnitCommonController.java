package com.lym.unit.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lym.unit.client.UnitService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * 理解与交互技术
 * @author liyingming
 *
 */
@Controller
@RequestMapping(value = "/unit")
public class UnitCommonController {
	
	/**
	 * unit对话首页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index.do")
	public ModelAndView queryUnit() throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/unit/unitIndex");
		return modelAndView;
	}

	/**
	 * UNIT 处理公共方法
	 * @author liyingming
	 * @return
	 */
	@RequestMapping(value = "/common.do")
	@ResponseBody
	public Map<String, Object> save(String msg,String session_id){
		Map<String, Object> modelMap = new HashMap<String, Object>();
		try {
			//判断文本是否为空
			if(!msg.equals("")){
					//调用unit分析语音
					Map<String, String> map = UnitService.utterance(session_id, msg);
					//判断是否命中意图
					if(!"fail_action".equals(map.get("action_id"))){
						//判断是否意图结束
						if("satisfy".equals(map.get("act_type"))){
							session_id="";//意图结束，清空session_id
							/**
							 * 以下是意图处理逻辑，根据命中的意图，判断进入当前意图的对话处理  current_qu_intent字段为意图
							 */
							switch (map.get("current_qu_intent")) {
								case "RAIN": //查询是否下雨
									//解析是否刮风的词槽列表
									String companyParams = getCompany(map.get("bot_merged_slots"));
									if(!companyParams.equals("")){
										modelMap.put("companyParams", companyParams);
									}else{
										modelMap.put("companyParams", map.get("raw_query"));
									}
									modelMap.put("queryType", "COMPANY");
									break;
								case "WIND": //查询是否刮风
									//解析是否刮风的词槽列表
									String companyParams2 = getCompany(map.get("bot_merged_slots"));
									if(!companyParams2.equals("")){
										modelMap.put("companyParams", companyParams2);
									}else{
										modelMap.put("companyParams", map.get("raw_query"));
									}
									modelMap.put("queryType", "COMPANY");
									break;
								default:
									break;
								}
						}else{
							//意图未完成 需要传入session_id 用户下一次对话
							session_id = map.get("session_id");
						}
						//返回机器人说的bot  不管意图有没有命中
						modelMap.put("msg", map.get("say"));
					}else{
						modelMap.put("msg", "我好像不太明白");
					}
				}
			modelMap.put("success", true);
			modelMap.put("session_id", session_id);
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.put("success", false);
			modelMap.put("data", e.getMessage());
        }
		return modelMap;
	}
	
	/**
	 * 解析是否刮风的词槽列表
	 * @param strJson
	 * @return
	 */
	private String getCompany(String strJson){
		String companyParams = "";
		JSONArray json2 = JSONArray.fromObject(strJson);
		// 遍历 jsonarray 数组，把每一个对象转成 json 对象
		if(json2.size()>0){
			for (Object object2 : json2) {
				JSONObject job2 =(JSONObject) object2;
	        	String type = (String) job2.get("type");
	        	if("user_guide".equals(type)){
	        		if(!((String) job2.get("normalized_word")).equals("")){
	        			companyParams = (String) job2.get("normalized_word");
	        		}else{
	        			companyParams = (String) job2.get("original_word");
	        		}
	        	}
			}
		}
		return companyParams;
	}
}
